### Message event processor

![alt text](event_processor_flow.png "workflow")

From one side we have events that should be processed by Event processor.
This processing should be done in the following way:
- read events
- validate
- aggregate(by event emitter) and sort (by emitter time and also sort events for this emitter by time)

###### event example:
```json
{
  "id": "1234",
  "time": "2019-10-12T12:50:48Z",
  "emitter": {
    "id": "1111",
    "name": "name",
    "register": "2019-09-20T08:20:15Z"
  },
  "product": {
    "id": "9999",
    "name": "productName",
    "price": 12345
  }
}
```

###### aggregation example:
```json
[
    {
      "emitter_name": "name",
      "register": "2019-09-20T08:20:15Z",
      "events": [
          {
            "product_name": "productName",
            "time": "2019-09-20T08:20:15Z"
          },
          ...
      ],
      "totalPrice": 12345
    },
    ...
]
```