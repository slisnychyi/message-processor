package com.slisnychyi.serivce;

import com.slisnychyi.model.Emitter;
import com.slisnychyi.model.Event;
import com.slisnychyi.model.Product;
import org.apache.commons.lang3.ObjectUtils;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;

public class EventValidator {

    boolean isValid(Event event) {
        return ofNullable(event)
                .filter(e -> ObjectUtils.allNotNull(e.getId(), e.getTime()))
                .filter(e -> isValidProduct(e.getProduct()))
                .filter(e -> isValidEmitter(e.getEmitter()))
                .map(Event::getProduct)
                .map(e -> e.getPrice() > 100)
                .orElse(false);
    }

    private boolean isValidProduct(Product product) {
        return ofNullable(product)
                .map(e -> allNotNull(e.getId(), e.getPrice(), e.getName()))
                .orElse(false);
    }

    private boolean isValidEmitter(Emitter emitter) {
        return ofNullable(emitter)
                .map(e -> allNotNull(e.getId(), e.getName(), e.getRegister()))
                .orElse(false);
    }

}
