package com.slisnychyi.serivce;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface EventProcessor {

    void process(InputStream source, OutputStream sink) throws IOException;

}
