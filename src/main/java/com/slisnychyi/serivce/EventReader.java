package com.slisnychyi.serivce;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.slisnychyi.model.Event;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import static com.fasterxml.jackson.annotation.PropertyAccessor.FIELD;
import static java.util.Optional.ofNullable;
import static java.util.concurrent.TimeUnit.SECONDS;

public class EventReader {

    private final ObjectMapper objectMapper;

    public EventReader() {
        objectMapper = new ObjectMapper()
                .setVisibility(FIELD, JsonAutoDetect.Visibility.ANY)
                .registerModule(new JavaTimeModule());
    }

    List<Event> readEvents(InputStream inputStream, int eventLimit, long maxTime) {
        List<Event> result = new ArrayList<>();
        AtomicInteger maxEvents = new AtomicInteger(eventLimit);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        CompletableFuture<Void> readerTask = CompletableFuture.runAsync(() -> {
            while (maxEvents.getAndDecrement() != 0) {
                ofNullable(readEvent(reader))
                        .ifPresent(result::add);
            }
        });
        try {
            readerTask.get(maxTime, SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ignored) {
        } finally {
            readerTask.cancel(true);
        }
        return result;
    }

    private Event readEvent(BufferedReader reader) {
        Event result = null;
        try {
            String line = reader.readLine();
            result = objectMapper.readValue(line, Event.class);
        } catch (IOException e) {
//            LOGGER.error("Exception while reading event. cause = {}", e.getMessage());
        }
        return result;
    }
}