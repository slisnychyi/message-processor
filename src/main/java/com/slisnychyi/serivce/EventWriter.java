package com.slisnychyi.serivce;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.slisnychyi.model.ResultEvent;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class EventWriter {

    private final ObjectMapper objectMapper;

    public EventWriter() {
        this.objectMapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());
    }

    public void writeEvents(OutputStream sink, List<ResultEvent> events) throws IOException {
        objectMapper.writeValue(sink, events);
    }
}
