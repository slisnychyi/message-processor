package com.slisnychyi.serivce;

import com.slisnychyi.model.Event;
import com.slisnychyi.model.ResultEvent;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultEventProcessor implements EventProcessor {

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    private final int maxEvents;
    private final long maxTime;

    private final EventReader reader;
    private final EventValidator validator;
    private final EventAggregator aggregator;
    private final EventWriter writer;


    public DefaultEventProcessor(int maxEvents, long maxTime,
                                 EventReader reader, EventValidator validator,
                                 EventAggregator aggregator, EventWriter writer) {
        this.maxEvents = maxEvents;
        this.maxTime = maxTime;
        this.reader = reader;
        this.validator = validator;
        this.aggregator = aggregator;
        this.writer = writer;
    }

    @Override
    public void process(InputStream source, OutputStream sink) throws IOException {
        List<Event> events = reader.readEvents(source, maxEvents, maxTime).stream()
                .filter(validator::isValid)
                .collect(Collectors.toList());
        List<ResultEvent> result = aggregator.aggregate(events);
        writer.writeEvents(sink, result);
    }
}