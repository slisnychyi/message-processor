package com.slisnychyi.serivce;

import com.slisnychyi.model.Emitter;
import com.slisnychyi.model.Event;
import com.slisnychyi.model.Purchase;
import com.slisnychyi.model.ResultEvent;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toCollection;

public class EventAggregator {


    List<ResultEvent> aggregate(List<Event> events) {
        return events.stream()
                .collect(groupingBy(Event::getEmitter, TreeMap::new,
                        mapping(Purchase::fromEvent, toCollection(TreeSet::new))))
                .entrySet().stream()
                .map(this::getEvent)
                .collect(Collectors.toList());
    }

    private ResultEvent getEvent(Map.Entry<Emitter, TreeSet<Purchase>> emitterPurchases) {
        Emitter emitter = emitterPurchases.getKey();
        return new ResultEvent(emitter.getName(), emitter.getRegister(), emitterPurchases.getValue());
    }
}