package com.slisnychyi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

import static com.slisnychyi.serivce.DefaultEventProcessor.DATE_FORMAT;

@Data
@AllArgsConstructor
public class ResultEvent {

    @JsonProperty("emitter_name")
    private String emitterName;
    @JsonFormat(pattern = DATE_FORMAT)
    private LocalDateTime register;
    private Set<Purchase> events;

}
