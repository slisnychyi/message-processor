package com.slisnychyi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

import static com.slisnychyi.serivce.DefaultEventProcessor.DATE_FORMAT;

@Data
@Builder
public class Event implements Comparable<LocalDateTime> {

    private String id;
    @JsonFormat(pattern = DATE_FORMAT)
    private LocalDateTime time;
    private Emitter emitter;
    private Product product;

    @Override
    public int compareTo(LocalDateTime o) {
        return time.compareTo(o);
    }
}
