package com.slisnychyi.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

import static com.slisnychyi.serivce.DefaultEventProcessor.DATE_FORMAT;

@Data
@Builder
public class Purchase implements Comparable<Purchase> {

    @JsonProperty("product_name")
    private String productName;
    @JsonFormat(pattern = DATE_FORMAT)
    private LocalDateTime time;

    public static Purchase fromEvent(Event event) {
        return new Purchase(event.getProduct().getName(), event.getTime());
    }

    @Override
    public int compareTo(Purchase o) {
        return time.compareTo(o.getTime());
    }
}
