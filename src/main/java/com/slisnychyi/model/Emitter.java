package com.slisnychyi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDateTime;
import java.util.Optional;

@Data
@Builder
public class Emitter implements Comparable<Emitter> {

    private String id;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private LocalDateTime register;

    @Override
    public int compareTo(Emitter picker) {
        return Optional.ofNullable(picker)
                .map(Emitter::getRegister)
                .map(e -> register.compareTo(e))
                .orElseThrow(() -> new IllegalStateException("Emitter can't be without activation time."));
    }
}
